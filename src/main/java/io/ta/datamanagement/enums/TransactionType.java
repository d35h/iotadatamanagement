package io.ta.datamanagement.enums;

public enum TransactionType {
    INCOMING("INCOMING"),
    OUTGOING("OUTGOING");

    private String value;

    TransactionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
