package io.ta.datamanagement.enums;

public enum TransactionState {
    OPEN("OPEN"),
    CLOSED("CLOSED");

    private String value;

    TransactionState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
