package io.ta.datamanagement.repository;

import io.ta.datamanagement.domain.Device;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Device entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {

    @Query("select device from Device device where device.deviceUserId.login = ?#{principal.username}")
    List<Device> findByDeviceUserIdIsCurrentUser();

}
