package io.ta.datamanagement.repository;

import io.ta.datamanagement.domain.Transaction;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Transaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("select transaction from Transaction transaction where transaction.userId.login = ?#{principal.username}")
    List<Transaction> findByUserIdIsCurrentUser();

}
