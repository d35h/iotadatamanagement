package io.ta.datamanagement.repository;

import io.ta.datamanagement.domain.SaleRoute;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the SaleRoute entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SaleRouteRepository extends JpaRepository<SaleRoute, Long> {

    @Query("select sale_route from SaleRoute sale_route where sale_route.userSale.login = ?#{principal.username}")
    List<SaleRoute> findByUserSaleIsCurrentUser();

}
