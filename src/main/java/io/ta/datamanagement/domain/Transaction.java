package io.ta.datamanagement.domain;

import io.ta.datamanagement.enums.TransactionState;
import io.ta.datamanagement.enums.TransactionType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "transaction_state")
    private String transactionState;

    @Column(name = "paid_amount", precision=10, scale=2)
    private BigDecimal paidAmount;

    @Column(name = "transaction_type")
    private String transactionType;

    @Column(name = "transaction_tag")
    private String transactionTag;

    @Column(name = "created_on")
    private Instant createdOn = Instant.now();

    @Column(name = "modified_on")
    private Instant modifiedOn = Instant.now();

    @ManyToOne
    private User userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionState() {
        return transactionState;
    }

    public Transaction transactionState(TransactionState transactionState) {
        this.transactionState = transactionState.getValue();
        return this;
    }

    public void setTransactionState(TransactionState transactionState) {
        this.transactionState = transactionState.getValue();
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public Transaction paidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
        return this;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public Transaction transactionType(TransactionType transactionType) {
        this.transactionType = transactionType.getValue();
        return this;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType.getValue();
    }

    public String getTransactionTag() {
        return transactionTag;
    }

    public Transaction transactionTag(String transactionTag) {
        this.transactionTag = transactionTag;
        return this;
    }

    public void setTransactionTag(String transactionTag) {
        this.transactionTag = transactionTag;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public Transaction createdOn(Instant createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public Instant getModifiedOn() {
        return modifiedOn;
    }

    public Transaction modifiedOn(Instant modifiedOn) {
        this.modifiedOn = modifiedOn;
        return this;
    }

    public void setModifiedOn(Instant modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public User getUserId() {
        return userId;
    }

    public Transaction userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transaction transaction = (Transaction) o;
        if (transaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", transactionState='" + getTransactionState() + "'" +
            ", paidAmount=" + getPaidAmount() +
            ", transactionType='" + getTransactionType() + "'" +
            ", transactionTag='" + getTransactionTag() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", modifiedOn='" + getModifiedOn() + "'" +
            "}";
    }
}
