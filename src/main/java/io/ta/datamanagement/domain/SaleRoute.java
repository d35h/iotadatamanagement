package io.ta.datamanagement.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SaleRoute.
 */
@Entity
@Table(name = "sale_route")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SaleRoute implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "generated_name")
    private String generatedName;

    @Column(name = "jhi_type")
    private String type;

    @ManyToOne
    private User userSale;

    @ManyToOne
    private Device deviceSales;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGeneratedName() {
        return generatedName;
    }

    public SaleRoute generatedName(String generatedName) {
        this.generatedName = generatedName;
        return this;
    }

    public void setGeneratedName(String generatedName) {
        this.generatedName = generatedName;
    }

    public String getType() {
        return type;
    }

    public SaleRoute type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUserSale() {
        return userSale;
    }

    public SaleRoute userSale(User user) {
        this.userSale = user;
        return this;
    }

    public void setUserSale(User user) {
        this.userSale = user;
    }

    public Device getDeviceSales() {
        return deviceSales;
    }

    public SaleRoute deviceSales(Device device) {
        this.deviceSales = device;
        return this;
    }

    public void setDeviceSales(Device device) {
        this.deviceSales = device;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SaleRoute saleRoute = (SaleRoute) o;
        if (saleRoute.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), saleRoute.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SaleRoute{" +
            "id=" + getId() +
            ", generatedName='" + getGeneratedName() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
