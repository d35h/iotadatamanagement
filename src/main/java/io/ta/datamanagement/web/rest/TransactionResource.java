package io.ta.datamanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.ta.datamanagement.service.TransactionService;
import io.ta.datamanagement.web.rest.errors.BadRequestAlertException;
import io.ta.datamanagement.web.rest.util.HeaderUtil;
import io.ta.datamanagement.service.dto.TransactionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Transaction.
 */
@RestController
@RequestMapping("/api")
public class TransactionResource {

    private final Logger log = LoggerFactory.getLogger(TransactionResource.class);

    private final TransactionService transactionService;

    public TransactionResource(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping("/getQrCode")
    @Timed
    public ResponseEntity<byte[]> getQrCode() {
        log.info("Generating QR code for transaction...");
        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(transactionService.saveTransactionAndGenerateQrCode());
    }
}
