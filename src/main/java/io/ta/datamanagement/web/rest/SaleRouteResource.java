package io.ta.datamanagement.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.ta.datamanagement.service.SaleRouteService;
import io.ta.datamanagement.web.rest.errors.BadRequestAlertException;
import io.ta.datamanagement.web.rest.util.HeaderUtil;
import io.ta.datamanagement.web.rest.util.PaginationUtil;
import io.ta.datamanagement.service.dto.SaleRouteDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SaleRoute.
 */
@RestController
@RequestMapping("/api")
public class SaleRouteResource {

    private final Logger log = LoggerFactory.getLogger(SaleRouteResource.class);

    private static final String ENTITY_NAME = "saleRoute";

    private final SaleRouteService saleRouteService;

    public SaleRouteResource(SaleRouteService saleRouteService) {
        this.saleRouteService = saleRouteService;
    }

    /**
     * POST  /sale-routes : Create a new saleRoute.
     *
     * @param saleRouteDTO the saleRouteDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new saleRouteDTO, or with status 400 (Bad Request) if the saleRoute has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sale-routes")
    @Timed
    public ResponseEntity<SaleRouteDTO> createSaleRoute(@RequestBody SaleRouteDTO saleRouteDTO) throws URISyntaxException {
        log.debug("REST request to save SaleRoute : {}", saleRouteDTO);
        if (saleRouteDTO.getId() != null) {
            throw new BadRequestAlertException("A new saleRoute cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SaleRouteDTO result = saleRouteService.save(saleRouteDTO);
        return ResponseEntity.created(new URI("/api/sale-routes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sale-routes : Updates an existing saleRoute.
     *
     * @param saleRouteDTO the saleRouteDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated saleRouteDTO,
     * or with status 400 (Bad Request) if the saleRouteDTO is not valid,
     * or with status 500 (Internal Server Error) if the saleRouteDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sale-routes")
    @Timed
    public ResponseEntity<SaleRouteDTO> updateSaleRoute(@RequestBody SaleRouteDTO saleRouteDTO) throws URISyntaxException {
        log.debug("REST request to update SaleRoute : {}", saleRouteDTO);
        if (saleRouteDTO.getId() == null) {
            return createSaleRoute(saleRouteDTO);
        }
        SaleRouteDTO result = saleRouteService.save(saleRouteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, saleRouteDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sale-routes : get all the saleRoutes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of saleRoutes in body
     */
    @GetMapping("/sale-routes")
    @Timed
    public ResponseEntity<List<SaleRouteDTO>> getAllSaleRoutes(Pageable pageable) {
        log.debug("REST request to get a page of SaleRoutes");
        Page<SaleRouteDTO> page = saleRouteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sale-routes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sale-routes/:id : get the "id" saleRoute.
     *
     * @param id the id of the saleRouteDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the saleRouteDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sale-routes/{id}")
    @Timed
    public ResponseEntity<SaleRouteDTO> getSaleRoute(@PathVariable Long id) {
        log.debug("REST request to get SaleRoute : {}", id);
        SaleRouteDTO saleRouteDTO = saleRouteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(saleRouteDTO));
    }

    /**
     * DELETE  /sale-routes/:id : delete the "id" saleRoute.
     *
     * @param id the id of the saleRouteDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sale-routes/{id}")
    @Timed
    public ResponseEntity<Void> deleteSaleRoute(@PathVariable Long id) {
        log.debug("REST request to delete SaleRoute : {}", id);
        saleRouteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
