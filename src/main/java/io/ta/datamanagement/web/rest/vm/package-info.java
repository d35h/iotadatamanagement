/**
 * View Models used by Spring MVC REST controllers.
 */
package io.ta.datamanagement.web.rest.vm;
