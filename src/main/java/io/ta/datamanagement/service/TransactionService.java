package io.ta.datamanagement.service;

import io.ta.datamanagement.domain.Transaction;
import io.ta.datamanagement.domain.User;
import io.ta.datamanagement.enums.TransactionState;
import io.ta.datamanagement.enums.TransactionType;
import io.ta.datamanagement.repository.TransactionRepository;
import io.ta.datamanagement.service.dto.TransactionDTO;
import io.ta.datamanagement.service.mapper.TransactionMapper;
import io.ta.datamanagement.service.util.QrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Transaction.
 */
@Service
@Transactional
public class TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionService.class);

    private final TransactionRepository transactionRepository;

    private final UserService userService;

    public TransactionService(TransactionRepository transactionRepository, UserService userService) {
        this.transactionRepository = transactionRepository;
        this.userService = userService;
    }

    public byte[] saveTransactionAndGenerateQrCode() {
        transactionRepository.save(createTransaction());
        return QrUtils.generateQrCodeImage().orElseGet(() -> new byte[0]);
    }

    private Transaction createTransaction() {
        Transaction transaction = new Transaction();
        transaction.setPaidAmount(BigDecimal.valueOf(0));
        transaction.setTransactionState(TransactionState.OPEN);
        transaction.setTransactionType(TransactionType.OUTGOING);
        transaction.setTransactionTag(UUID.randomUUID().toString());
        final User user = userService.getUserWithAuthorities().orElseThrow(IllegalArgumentException::new);
        transaction.setUserId(user);

        return transaction;
    }

}
