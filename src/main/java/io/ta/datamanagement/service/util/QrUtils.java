package io.ta.datamanagement.service.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;

public class QrUtils {

    private static final String JPG = "jpg";

    private static final int WIDTH = 450;
    private static final int HEIGHT = 450;

    public static Optional<byte[]> generateQrCodeImage() {
        try {
            return Optional.of(QrUtils.generateQrCodeImage(generateTextForQrCode(), WIDTH, HEIGHT));
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    private static byte[] generateQrCodeImage(String text, int width, int height)
        throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(MatrixToImageWriter.toBufferedImage(bitMatrix), JPG, baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();

        return imageInByte;
    }

    private static String generateTextForQrCode() {
        String wallet = "DPJDHKJBNKH12312HKJDAHJPOEQLKMBNG";
        String amount = "1";
        String message = "test_message";
        String tag = "test_tag";

        return "{" + "address:" + wallet + ","
            + "amount:" + amount + ","
            + "message:" + message + ","
            + "tag:" + tag + "}";
    }

}
