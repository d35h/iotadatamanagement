package io.ta.datamanagement.service;

import io.ta.datamanagement.domain.SaleRoute;
import io.ta.datamanagement.repository.SaleRouteRepository;
import io.ta.datamanagement.service.dto.SaleRouteDTO;
import io.ta.datamanagement.service.mapper.SaleRouteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing SaleRoute.
 */
@Service
@Transactional
public class SaleRouteService {

    private final Logger log = LoggerFactory.getLogger(SaleRouteService.class);

    private final SaleRouteRepository saleRouteRepository;

    private final SaleRouteMapper saleRouteMapper;

    private final DataLayerService dataLayerService;

    public SaleRouteService(SaleRouteRepository saleRouteRepository, SaleRouteMapper saleRouteMapper, DataLayerService dataLayerService) {
        this.saleRouteRepository = saleRouteRepository;
        this.saleRouteMapper = saleRouteMapper;
        this.dataLayerService = dataLayerService;
    }

    /**
     * Save a saleRoute.
     *
     * @param saleRouteDTO the entity to save
     * @return the persisted entity
     */
    public SaleRouteDTO save(SaleRouteDTO saleRouteDTO) {
        log.debug("Request to save SaleRoute : {}", saleRouteDTO);
        SaleRoute saleRoute = saleRouteMapper.toEntity(saleRouteDTO);
        saleRoute = saleRouteRepository.save(saleRoute);
        dataLayerService.addRoute(saleRoute);
        return saleRouteMapper.toDto(saleRoute);
    }

    /**
     * Get all the saleRoutes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SaleRouteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SaleRoutes");
        return saleRouteRepository.findAll(pageable)
            .map(saleRouteMapper::toDto);
    }

    /**
     * Get one saleRoute by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SaleRouteDTO findOne(Long id) {
        log.debug("Request to get SaleRoute : {}", id);
        SaleRoute saleRoute = saleRouteRepository.findOne(id);
        return saleRouteMapper.toDto(saleRoute);
    }

    /**
     * Delete the saleRoute by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SaleRoute : {}", id);
        dataLayerService.removeRoute(saleRouteRepository.findOne(id));
        saleRouteRepository.delete(id);
    }
}
