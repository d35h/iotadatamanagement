/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package io.ta.datamanagement.service.mapper;
