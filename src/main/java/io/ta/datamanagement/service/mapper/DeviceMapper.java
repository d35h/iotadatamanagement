package io.ta.datamanagement.service.mapper;

import io.ta.datamanagement.domain.*;
import io.ta.datamanagement.service.dto.DeviceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Device and its DTO DeviceDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface DeviceMapper extends EntityMapper<DeviceDTO, Device> {

    @Mapping(source = "deviceUserId.id", target = "deviceUserIdId")
    @Mapping(source = "deviceUserId.login", target = "deviceUserIdLogin")
    DeviceDTO toDto(Device device);

    @Mapping(source = "deviceUserIdId", target = "deviceUserId")
    Device toEntity(DeviceDTO deviceDTO);

    default Device fromId(Long id) {
        if (id == null) {
            return null;
        }
        Device device = new Device();
        device.setId(id);
        return device;
    }
}
