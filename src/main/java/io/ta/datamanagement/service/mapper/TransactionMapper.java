package io.ta.datamanagement.service.mapper;

import io.ta.datamanagement.domain.*;
import io.ta.datamanagement.service.dto.TransactionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Transaction and its DTO TransactionDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface TransactionMapper extends EntityMapper<TransactionDTO, Transaction> {

    @Mapping(source = "userId.id", target = "userIdId")
    @Mapping(source = "userId.login", target = "userIdLogin")
    TransactionDTO toDto(Transaction transaction);

    @Mapping(source = "userIdId", target = "userId")
    Transaction toEntity(TransactionDTO transactionDTO);

    default Transaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        Transaction transaction = new Transaction();
        transaction.setId(id);
        return transaction;
    }
}
