package io.ta.datamanagement.service.mapper;

import io.ta.datamanagement.domain.*;
import io.ta.datamanagement.service.dto.SaleRouteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SaleRoute and its DTO SaleRouteDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, DeviceMapper.class})
public interface SaleRouteMapper extends EntityMapper<SaleRouteDTO, SaleRoute> {

    @Mapping(source = "userSale.id", target = "userSaleId")
    @Mapping(source = "userSale.login", target = "userSaleLogin")
    @Mapping(source = "deviceSales.id", target = "deviceSalesId")
    SaleRouteDTO toDto(SaleRoute saleRoute);

    @Mapping(source = "userSaleId", target = "userSale")
    @Mapping(source = "deviceSalesId", target = "deviceSales")
    SaleRoute toEntity(SaleRouteDTO saleRouteDTO);

    default SaleRoute fromId(Long id) {
        if (id == null) {
            return null;
        }
        SaleRoute saleRoute = new SaleRoute();
        saleRoute.setId(id);
        return saleRoute;
    }
}
