package io.ta.datamanagement.service;

import io.ta.datamanagement.domain.SaleRoute;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DataLayerService {

//    @Value("datalayer.url")
    private String dataLayerUrl = "http://10.0.0.51:8089/api";
    private final static String ROUTE_ENDPOINT = "/route";

        // replace ewww code
    public String addRoute(SaleRoute saleRoute){
        System.out.println("trying to send" + dataLayerUrl );

        JSONObject request = new JSONObject();
        try {
            request.put("name", saleRoute.getGeneratedName());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> responseEntity = restTemplate
            .exchange(dataLayerUrl + ROUTE_ENDPOINT, HttpMethod.POST, entity, String.class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            return responseEntity.getBody();
        }
        } catch (JSONException e) {
        }
        return null;
    }

    public void removeRoute(SaleRoute saleRoute){

    }
}
