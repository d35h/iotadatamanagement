package io.ta.datamanagement.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the SaleRoute entity.
 */
public class SaleRouteDTO implements Serializable {

    private Long id;

    private String generatedName;

    private String type;

    private Long userSaleId;

    private String userSaleLogin;

    private Long deviceSalesId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGeneratedName() {
        return generatedName;
    }

    public void setGeneratedName(String generatedName) {
        this.generatedName = generatedName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getUserSaleId() {
        return userSaleId;
    }

    public void setUserSaleId(Long userId) {
        this.userSaleId = userId;
    }

    public String getUserSaleLogin() {
        return userSaleLogin;
    }

    public void setUserSaleLogin(String userLogin) {
        this.userSaleLogin = userLogin;
    }

    public Long getDeviceSalesId() {
        return deviceSalesId;
    }

    public void setDeviceSalesId(Long deviceId) {
        this.deviceSalesId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SaleRouteDTO saleRouteDTO = (SaleRouteDTO) o;
        if(saleRouteDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), saleRouteDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SaleRouteDTO{" +
            "id=" + getId() +
            ", generatedName='" + getGeneratedName() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
