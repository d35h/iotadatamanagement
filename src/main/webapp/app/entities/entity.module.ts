import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { IotaDataManagementDeviceModule } from './device/device.module';
import { IotaDataManagementSaleRouteModule } from './sale-route/sale-route.module';
import { IotaDataManagementTransactionModule } from './transaction/transaction.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        IotaDataManagementDeviceModule,
        IotaDataManagementSaleRouteModule,
        IotaDataManagementTransactionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IotaDataManagementEntityModule {}
