import { BaseEntity } from './../../shared';

export class Device implements BaseEntity {
    constructor(
        public id?: number,
        public deviceId?: string,
        public name?: string,
        public type?: string,
        public deviceUserIdId?: number,
    ) {
    }
}
