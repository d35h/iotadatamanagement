import { BaseEntity } from './../../shared';

export class Transaction implements BaseEntity {
    constructor(
        public id?: number,
        public transactionState?: string,
        public paidAmount?: number,
        public transactionType?: string,
        public transactionTag?: string,
        public createdOn?: any,
        public modifiedOn?: any,
        public userIdId?: number,
    ) {
    }
}
