import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SaleRoute } from './sale-route.model';
import { SaleRoutePopupService } from './sale-route-popup.service';
import { SaleRouteService } from './sale-route.service';

@Component({
    selector: 'jhi-sale-route-delete-dialog',
    templateUrl: './sale-route-delete-dialog.component.html'
})
export class SaleRouteDeleteDialogComponent {

    saleRoute: SaleRoute;

    constructor(
        private saleRouteService: SaleRouteService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.saleRouteService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'saleRouteListModification',
                content: 'Deleted an saleRoute'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sale-route-delete-popup',
    template: ''
})
export class SaleRouteDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private saleRoutePopupService: SaleRoutePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.saleRoutePopupService
                .open(SaleRouteDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
