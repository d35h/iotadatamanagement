import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SaleRoute } from './sale-route.model';
import { SaleRoutePopupService } from './sale-route-popup.service';
import { SaleRouteService } from './sale-route.service';
import { User, UserService } from '../../shared';
import { Device, DeviceService } from '../device';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-sale-route-dialog',
    templateUrl: './sale-route-dialog.component.html'
})
export class SaleRouteDialogComponent implements OnInit {

    saleRoute: SaleRoute;
    isSaving: boolean;

    users: User[];

    devices: Device[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private saleRouteService: SaleRouteService,
        private userService: UserService,
        private deviceService: DeviceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.deviceService.query()
            .subscribe((res: ResponseWrapper) => { this.devices = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.saleRoute.id !== undefined) {
            this.subscribeToSaveResponse(
                this.saleRouteService.update(this.saleRoute));
        } else {
            this.subscribeToSaveResponse(
                this.saleRouteService.create(this.saleRoute));
        }
    }

    private subscribeToSaveResponse(result: Observable<SaleRoute>) {
        result.subscribe((res: SaleRoute) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SaleRoute) {
        this.eventManager.broadcast({ name: 'saleRouteListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackDeviceById(index: number, item: Device) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-sale-route-popup',
    template: ''
})
export class SaleRoutePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private saleRoutePopupService: SaleRoutePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.saleRoutePopupService
                    .open(SaleRouteDialogComponent as Component, params['id']);
            } else {
                this.saleRoutePopupService
                    .open(SaleRouteDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
