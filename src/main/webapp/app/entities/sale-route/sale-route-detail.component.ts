import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SaleRoute } from './sale-route.model';
import { SaleRouteService } from './sale-route.service';

@Component({
    selector: 'jhi-sale-route-detail',
    templateUrl: './sale-route-detail.component.html'
})
export class SaleRouteDetailComponent implements OnInit, OnDestroy {

    saleRoute: SaleRoute;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private saleRouteService: SaleRouteService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSaleRoutes();
    }

    load(id) {
        this.saleRouteService.find(id).subscribe((saleRoute) => {
            this.saleRoute = saleRoute;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSaleRoutes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'saleRouteListModification',
            (response) => this.load(this.saleRoute.id)
        );
    }
}
