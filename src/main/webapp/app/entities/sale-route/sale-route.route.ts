import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SaleRouteComponent } from './sale-route.component';
import { SaleRouteDetailComponent } from './sale-route-detail.component';
import { SaleRoutePopupComponent } from './sale-route-dialog.component';
import { SaleRouteDeletePopupComponent } from './sale-route-delete-dialog.component';

export const saleRouteRoute: Routes = [
    {
        path: 'sale-route',
        component: SaleRouteComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iotaDataManagementApp.saleRoute.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sale-route/:id',
        component: SaleRouteDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iotaDataManagementApp.saleRoute.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const saleRoutePopupRoute: Routes = [
    {
        path: 'sale-route-new',
        component: SaleRoutePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iotaDataManagementApp.saleRoute.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sale-route/:id/edit',
        component: SaleRoutePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iotaDataManagementApp.saleRoute.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sale-route/:id/delete',
        component: SaleRouteDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'iotaDataManagementApp.saleRoute.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
