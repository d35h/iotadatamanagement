import { BaseEntity } from './../../shared';

export class SaleRoute implements BaseEntity {
    constructor(
        public id?: number,
        public generatedName?: string,
        public type?: string,
        public userSaleId?: number,
        public deviceSalesId?: number,
    ) {
    }
}
