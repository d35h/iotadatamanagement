import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { IotaDataManagementSharedModule } from '../../shared';
import { IotaDataManagementAdminModule } from '../../admin/admin.module';
import {
    SaleRouteService,
    SaleRoutePopupService,
    SaleRouteComponent,
    SaleRouteDetailComponent,
    SaleRouteDialogComponent,
    SaleRoutePopupComponent,
    SaleRouteDeletePopupComponent,
    SaleRouteDeleteDialogComponent,
    saleRouteRoute,
    saleRoutePopupRoute,
} from './';

const ENTITY_STATES = [
    ...saleRouteRoute,
    ...saleRoutePopupRoute,
];

@NgModule({
    imports: [
        IotaDataManagementSharedModule,
        IotaDataManagementAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SaleRouteComponent,
        SaleRouteDetailComponent,
        SaleRouteDialogComponent,
        SaleRouteDeleteDialogComponent,
        SaleRoutePopupComponent,
        SaleRouteDeletePopupComponent,
    ],
    entryComponents: [
        SaleRouteComponent,
        SaleRouteDialogComponent,
        SaleRoutePopupComponent,
        SaleRouteDeleteDialogComponent,
        SaleRouteDeletePopupComponent,
    ],
    providers: [
        SaleRouteService,
        SaleRoutePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IotaDataManagementSaleRouteModule {}
