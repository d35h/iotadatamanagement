/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { IotaDataManagementTestModule } from '../../../test.module';
import { SaleRouteDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/sale-route/sale-route-delete-dialog.component';
import { SaleRouteService } from '../../../../../../main/webapp/app/entities/sale-route/sale-route.service';

describe('Component Tests', () => {

    describe('SaleRoute Management Delete Component', () => {
        let comp: SaleRouteDeleteDialogComponent;
        let fixture: ComponentFixture<SaleRouteDeleteDialogComponent>;
        let service: SaleRouteService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IotaDataManagementTestModule],
                declarations: [SaleRouteDeleteDialogComponent],
                providers: [
                    SaleRouteService
                ]
            })
            .overrideTemplate(SaleRouteDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SaleRouteDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SaleRouteService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
