/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { IotaDataManagementTestModule } from '../../../test.module';
import { SaleRouteDetailComponent } from '../../../../../../main/webapp/app/entities/sale-route/sale-route-detail.component';
import { SaleRouteService } from '../../../../../../main/webapp/app/entities/sale-route/sale-route.service';
import { SaleRoute } from '../../../../../../main/webapp/app/entities/sale-route/sale-route.model';

describe('Component Tests', () => {

    describe('SaleRoute Management Detail Component', () => {
        let comp: SaleRouteDetailComponent;
        let fixture: ComponentFixture<SaleRouteDetailComponent>;
        let service: SaleRouteService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IotaDataManagementTestModule],
                declarations: [SaleRouteDetailComponent],
                providers: [
                    SaleRouteService
                ]
            })
            .overrideTemplate(SaleRouteDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SaleRouteDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SaleRouteService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new SaleRoute(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.saleRoute).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
