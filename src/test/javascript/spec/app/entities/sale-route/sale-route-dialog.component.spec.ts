/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { IotaDataManagementTestModule } from '../../../test.module';
import { SaleRouteDialogComponent } from '../../../../../../main/webapp/app/entities/sale-route/sale-route-dialog.component';
import { SaleRouteService } from '../../../../../../main/webapp/app/entities/sale-route/sale-route.service';
import { SaleRoute } from '../../../../../../main/webapp/app/entities/sale-route/sale-route.model';
import { UserService } from '../../../../../../main/webapp/app/shared';
import { DeviceService } from '../../../../../../main/webapp/app/entities/device';

describe('Component Tests', () => {

    describe('SaleRoute Management Dialog Component', () => {
        let comp: SaleRouteDialogComponent;
        let fixture: ComponentFixture<SaleRouteDialogComponent>;
        let service: SaleRouteService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IotaDataManagementTestModule],
                declarations: [SaleRouteDialogComponent],
                providers: [
                    UserService,
                    DeviceService,
                    SaleRouteService
                ]
            })
            .overrideTemplate(SaleRouteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SaleRouteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SaleRouteService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SaleRoute(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.saleRoute = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'saleRouteListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SaleRoute();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.saleRoute = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'saleRouteListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
