/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { IotaDataManagementTestModule } from '../../../test.module';
import { SaleRouteComponent } from '../../../../../../main/webapp/app/entities/sale-route/sale-route.component';
import { SaleRouteService } from '../../../../../../main/webapp/app/entities/sale-route/sale-route.service';
import { SaleRoute } from '../../../../../../main/webapp/app/entities/sale-route/sale-route.model';

describe('Component Tests', () => {

    describe('SaleRoute Management Component', () => {
        let comp: SaleRouteComponent;
        let fixture: ComponentFixture<SaleRouteComponent>;
        let service: SaleRouteService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [IotaDataManagementTestModule],
                declarations: [SaleRouteComponent],
                providers: [
                    SaleRouteService
                ]
            })
            .overrideTemplate(SaleRouteComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SaleRouteComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SaleRouteService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new SaleRoute(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.saleRoutes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
